```sh
cd Circle

# Install dependencies
pip install -r scripts/requirements.txt

# Generate the html file
python scripts/gen_apps.py
cp -r assets output

# Start a web server
python -m http.server 8080 --bind 127.0.0.1 --directory output

# Open in the browser
xdg-open http://127.0.0.1:8080/
```
