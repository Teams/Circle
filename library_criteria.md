# GNOME Circle library criteria

 - [ ] Follows [GNOME coding guidelines](https://developer.gnome.org/documentation/guidelines/programming.html)
 - [ ] Targets glib-based applications
 - [ ] Targets interactive, end-user or graphical applications, or facilitates their development
 - [ ] Is used by at least one other component or app in GNOME Circle or Core
 - [ ] Documentation provided (at least API reference docs)
 - [ ] Works as expected without serious issues (verify this by referring to the project's issue tracker and/or contacting users of the library)
 - [ ] No GNOME branding

#### Repository

  - [ ] CI builds of the library
  - [ ] Public issue tracker and code repository

#### Legal

  - [ ] Has an [OSI-approved license](https://opensource.org/licenses)
  - [ ] No contributor license agreement (CLA)
  - [ ] The software must work without installing proprietary software

#### Recommended

  - [ ] Build system is Meson, Automake, or CMake

## Additional criteria for GObject language bindings

In addition to the criteria above, a library that provides language bindings to
the GObject stack must fulfill the following requirements:

  - [ ] A template of some sort that makes it easy for developers to get started using the bindings on hand.
  - [ ] Confidence in the maintainers' ability to update the bindings *at least every GNOME release cycle*

  - [ ] The bindings must be mainly auto-generated, although hand-written parts are also allowed.
    - [ ] The above-mentioned library criteria must also apply to the binding generator.

If the goal of the binding project is to provide close mappings to the C functions,
the following criteria must also be fulfilled:

  - [ ] Supports composite templates and subclassing (if the target language supports it)
  - [ ] Follows the [GObject Introspection guidelines](https://gi.readthedocs.io/en/latest/writingbindings/guidelines.html)

  - [ ] Fully supports the latest major version of the following libraries:
    - [ ] GObject
    - [ ] GTK
    - [ ] GLib/GIO
    - [ ] libadwaita

We recognize that in order to suit the coding style of different programming languages,
not all of these criteria can always be met.

We also don't want to exclude bindings that take some different, innovative approach,
so not all of the criteria might be relevant to a certain set of bindings.

In the end, we basically want to judge the bindings by the developer experience they provide,
so if in doubt, just open an application, and the Circle team will decide for that particular case.
