![Review Model](assets/review_model.png)

# GNOME Circle Review Procedure

This document contains information about how the GNOME Circle Committee operates, and how the quality of any given GNOME Circle member project is ensured, both initially and long-term. It is primarily intended as a reference for GNOME Circle Committee members, referred to as "reviewers" in this context, but may be of use to anyone interested.

## Choosing Projects to Review

Reviewers are generally free to choose which projects to review themselves. Deciding factors can be time at hand, experience with the domain of the project, and perceived readiness for Circle. For instance, a reviewer who is currently busy with work outside of Circle may choose to review a project that looks like it is almost ready for inclusion upon submission, or is simple in scope and contains few elements to inspect.

If possible, reviewers are advised to prioritize the projects and ongoing reviews that have gone the longest without seeing any interaction from the committee.

Reviewers can not review projects that they maintain themselves.

## Location

The review primarily takes place in the thread tied to the membership application of the project on GNOME's GitLab instance. It is not required for all communication between the reviewer and applicant to happen here, but the thread should at a minimum provide an overview of everything that needs to be worked on for the project to be eligible for inclusion in Circle, including the full review checklist once it has been gone through by the reviewer.

## Initial Interaction

The reviewer should attempt to establish a positive and constructive tone from the start. Here are some suitable elements for the initial message:

- A short and polite greeting (for example: "Hi, and thanks for your interest in GNOME Circle!")
- An indication of how much work is required for the project to be ready for inclusion (for example: "Just to set expectations for the review, the app will need some work before it's ready for inclusion.")
- A short list of quick and high-visibility improvements that can be made to the project's metadata, UI strings, and styling

It is not required for the full review checklist to be gone through instantly. The point this is done at is at the discretion of the reviewer.

## Testing an App

The reviewer should use the latest development version of the app built from source when testing. If it is not possible to build the app, the applicant should be notified about it and subsequently fix it or provide the necessary guidance to perform a build.

When first testing the app, the reviewer is advised to perform various actions to get a good overview of the following, if the app has them:

- High-level UI issues
- Instability and crashes
- Small quirks that appear in given situations

These should be put into words as concisely as possible and communicated to the applicant in a tidy manner.

## Going Through the Criteria

There are two checklists, one for apps, and one for components/libraries. The checklists contain the formal criteria for projects to be eligible for inclusion in Circle. The checklist for apps is located in the [App Organization][app-org] repository, while the checklist for components is located in the [Circle][circle] repository itself.

In the app checklist, the reviewer should pay extra attention to these criteria:

- Basic features and functionality work as expected
- Easy to get started on first run
- Follows the Human Interface Guidelines

These are particularly wide in scope, and whether the app meets them is at the sole discretion of the reviewer.

The reviewer should go through the checklist criterion-by-criterion, and check the boxes for the ones the project fulfills. After this, the list should be presented as-is to the applicant, accompanied by actionable steps that can be taken to meet the remaining criteria.

To avoid cluttering the thread, this syntax can be used:

```html
<details>
<summary>Press to view full checklist</summary>

[insert checklist here]

</details>
```

If the project still does not seem ready for inclusion to the reviewer even after having met all the criteria, the reviewer should go through the criteria again and see which ones the project may not quite meet. If the criteria turn out to not cover an aspect of the project that is deemed to be a blocker for inclusion, improvements to the criteria should be initiated as soon as possible. After this, the review of the app should continue as normal with the revised criteria.

## Cross-Review

When one reviewer has deemed a project ready for inclusion, another committee member assumes the role as cross-reviewer, and conducts an independent review of the project. This includes taking brief look at the project and going through the checklist. When the cross-reviewer has given their approval, the project is ready for inclusion.

## Inclusion

The canonical lists of Circle projects are the [apps.json][app-list] and [libs.json][lib-list] files in the GNOME Circle repository. To formally add an app, this needs to be appended to the app list:

```json
{
  "app_id": "org.example.AppName",
  "added": "YYYY-MM-DD",
  "last_review": "YYYY-MM-DD"
}
```

To formally add a component, this needs to be appended to the library list:

```json
{
    "name": "library-name",
    "url": "https://example.org"
}
```

The indentation should be kept consistent, and commas should be applied correctly according to JSON syntax rules. The last element of the list should not be followed by a comma.

The [gnome-apps.txt][gnome-software-list] file in the gnome-app-list project is used by GNOME Software to display featured apps. It contains all GNOME Circle apps, and should be kept in sync with the GNOME Circle app list.

## Keeping up Post-Inclusion

In accordance with the [Membership Guide][member-guide], Circle projects should continue to meet all the criteria after inclusion. 

Due to limited capacity, there are no reoccuring reviews of projects at set intervals. Instead, issues and deviations from the criteria are handled as they are noticed, usually by opening issues in the source code repositories of the projects, mentioning the GNOME Circle criterion being breached. The wider community is encouraged to assist the committee by being on the outlook for deviations, reporting them, and contacting the committee if deemed necessary.

### Control Reviews

If a project appears to deviate significantly from the standards of GNOME Circle, a control review can be initiated. This is a full re-review of the app, following the same procedure as an initial review. Control reviews have a deadline set on a case-by-case basis by the committee. If the app does not pass the review before the deadline passes, the app is excluded from Circle. It can still apply for inclusion again later, and will then be treated as any other app.

### Group Notices

A group notice goes out to a list of apps that all breach the same criterion. This can happen for reasons such as a GNOME runtime version reaching end-of-life, or a new criterion being added that needs conscious effort to be met. Like control reviews, group notices have a deadline set on a case-by-case basis by the committee. Apps that do not fulfill the relevant criterion before the deadline passes, are excluded from Circle. They can still apply for inclusion again later, and will then be treated as any other app.

---

Any questions regarding this document should be directed to the [GNOME Circle chat room][circle-chat] on Matrix. Changes can be suggested by opening an issue against this repository or directly with a merge request.

[circle]: https://gitlab.gnome.org/Teams/Circle
[app-org]: https://gitlab.gnome.org/Teams/Releng/AppOrganization
[app-list]: https://gitlab.gnome.org/Teams/Circle/-/blob/main/data/apps.json
[lib-list]: https://gitlab.gnome.org/Teams/Circle/-/blob/main/data/libs.json
[gnome-software-list]: https://gitlab.gnome.org/GNOME/gnome-app-list/-/blob/main/data/gnome-apps.txt
[member-guide]: https://gitlab.gnome.org/Teams/Circle/-/blob/main/membership_guide.md
[circle-chat]: https://matrix.to/#/#circle:gnome.org
