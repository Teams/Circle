<!--

Thanks for your interest in GNOME Circle!

Before submitting your app, please try to make sure that the app fulfills
most of the "General App Criteria" and "Circle App Criteria" linked below.

Remaining uncertainties can be sorted out during the submission process or
you can contact us in the #circle:gnome.org Matrix channel.

- https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/AppCriteria.md

Please also make yourself familiar with what we expect of GNOME Circle project maintainers.

- https://gitlab.gnome.org/Teams/Circle/-/blob/HEAD/membership_guide.md#expectations

Issue title: New app: <app name>

-->

##### App information

  - App name:
  - Code repository page:
  - Flathub page:

##### Code of Conduct

GNOME Circle projects are expected to follow [GNOME's Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct).

<!--

Please have a look at GNOME's Code of Conduct and establish a consensus about
following GNOME's Code of Conduct within the app's project. Afterward, confirm
the following statement by activating the checkbox.

-->

- [ ] I confirm the project's willingness to follow the GNOME Code of Conduct and to assist the GNOME's Code of Conduct Committee if required.

##### Pre-review checklist

In order to speed up reviews, please make sure you do some quality checks yourself and fix resulting issues before applying. Test every part of your app with each of the following:

- Keyboard-only
- Screen Reader (enable Orca with `Alt+Super+S`)
- Dark Mode
- High Contrast
- Large Text

Please ensure the following things in particular:

- [ ] Every part of the app UI can be navigated to and used with just the keyboard
- [ ] All UI elements have visible (and polished-looking) focus borders or other focus indicators
- [ ] All icon-only buttons have a tooltip
- [ ] All UI elements and content in the app read out in a useful way in Orca
- [ ] The app uses the [standard keyboard shortcuts](https://developer.gnome.org/hig/reference/keyboard.html), including
  - `Ctrl+Q` and `Ctrl+W` for closing all windows and the current window or tab respectively. For single-window apps both should work.
  - `Ctrl+,` for the preferences window
  - `F10` for opening the primary/secondary menu
  - `Ctrl+?` for keyboard shortcuts

Also ensure the repository meets the following criteria:

- [ ] The app has CI set up
- [ ] The repository contains a .doap file with maintainers
- [ ] The README mentions the GNOME Code of Conduct