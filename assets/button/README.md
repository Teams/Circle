# GNOME Circle badges

Member projects can use GNOME Circle badges on their website.

#### badge

`https://circle.gnome.org/assets/button/badge.svg`

![](https://circle.gnome.org/assets/button/badge.svg)

#### circle-button-fullcolor

`https://circle.gnome.org/assets/button/circle-button-fullcolor.svg`

![](https://circle.gnome.org/assets/button/circle-button-fullcolor.svg)

#### circle-button-i

`https://circle.gnome.org/assets/button/circle-button-i.svg`

![](https://circle.gnome.org/assets/button/circle-button-i.svg)

#### circle-button

`https://circle.gnome.org/assets/button/circle-button.svg`

![](https://circle.gnome.org/assets/button/circle-button.svg)
